window.onload = inicializar ;
var from_validasi;
var mahasiswaRef;
var tableValidasi;
var CREATE = "Create";
var UPDATE ="Update";
var status=CREATE;
var editRef;

function inicializar(){
	from_validasi = document.getElementById("from-validasi");
	from_validasi.addEventListener("submit", validasiFirebase , false);
	tableValidasi =document.getElementById("tbody"); 
	mahasiswaRef = firebase.database().ref().child("Kontrol");
	tampil();
}

function tampil(){

	mahasiswaRef.on("value", function(snapshot)
	{
		var data = snapshot.val();
		var files ="";
		for(var key in data)
		{
			files += "<tr>"+
							   
							   "<td>" + data[key].email + "</td>"+
							   "<td>" + data[key].jam + "</td>"+
							   "<td>" + data[key].tanggal + "</td>"+
							   '<td>'+'<button class="btn btn-primary edit" data-validasi="'+key+'">Edit</button>'+'</td>' +
							   '<td>'+'<button class="btn btn-danger hapus" data-validasi="'+key+'">Hapus</button>'+'</td>' +
							  "<tr>";
			

		}
		tableValidasi.innerHTML= files;
		if (files !="") {	

			var elementEditTabel = document.getElementsByClassName("edit");
			for (var i =0; i<elementEditTabel.length; i++) {
				elementEditTabel[i].addEventListener("click",editaFirebase,false);			}


			var elementHapusTable = document.getElementsByClassName("hapus");
			for (var i =0; i<elementHapusTable.length; i++) {
				elementHapusTable[i].addEventListener("click",hapusFirebase,false);			}
		}
	});
}

function editaFirebase(){
	var keyEdit = this.getAttribute("data-validasi");
	editRef = mahasiswaRef.child(keyEdit);
	editRef.once("value", function(snap){
		var data = snap.val();
		document.getElementById("email_id").value = data.email;
		document.getElementById("jam_id").value = data.jam;
		document.getElementById("tanggal_id").value = data.tanggal;
	});
	document.getElementById("buttom-form").value =UPDATE;
	status = UPDATE;
	
}

function hapusFirebase(){
	var keyHapus = this.getAttribute("data-validasi");
	var hapusRef = mahasiswaRef.child(keyHapus);
	hapusRef.remove();
}



function validasiFirebase(event){
	event.preventDefault();

    switch(status)
    {
    	case CREATE:	
	    mahasiswaRef.push({
		email : event.target.email.value,
		jam : event.target.jam.value,
		tanggal : event.target.tanggal.value,
	});
	break;
		case UPDATE:
		editRef.update({
		email : event.target.email.value,
		jam : event.target.jam.value,
		tanggal : event.target.tanggal.value,
		})
      document.getElementById("buttom-form").value =UPDATE;

    }
	from_validasi.reset();
}
